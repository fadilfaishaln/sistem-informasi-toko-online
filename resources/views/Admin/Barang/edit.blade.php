@extends('Admin.layouts.index')

@section('content')
    <div class="col-md-12">
        <h2><i class="fas fa-edit"></i> | Edit {{ $barang->nama_barang }}</h2>
        <form action="/UpdateBarang/{{ $barang->id }}" method="post" enctype="multipart/form-data">
        @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" class="form-control" name="nama" id="nama" value="{{ $barang->nama_barang }}">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <img src="{{ asset('img/'.$barang->foto_barang) }}"  height="114px" width="140px">
                        </div>
                        <div class="form-group col-md-8">
                            <label>Ganti Foto</label>
                            <input type="file" name="foto_barang" id="foto_barang" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Harga Barang</label>
                        <input type="number" class="form-control" name="harga" id="harga" value="{{ $barang->harga_barang }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Stok</label>
                        <select name="stok" id="stok" class="form-control">
                            <option value="0">- Pilih Stok -</option>
                            <option value="ada" @if($barang->stok == 'ada') : selected ? "" @endif>Ada</option>
                            <option value="habis" @if($barang->stok == 'habis') : selected ? ""  @endif>Habis</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea class="form-control" name="ket" id="ket" rows="3">{{$barang->keterangan}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Pesan</label>
                        <input type="text" class="form-control" name="pesan" id="pesan" value="{{ $barang->pesan }}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="{{ route('Barang') }}" class="btn btn-danger"><i class="fa fa-close"> Batal</i></a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </form>
    </div>
@endsection