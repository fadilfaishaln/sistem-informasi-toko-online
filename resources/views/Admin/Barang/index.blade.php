@extends('Admin.layouts.index')

@section('content')
    <div class="col-md-12">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(\Session::has('status-save'))
            <div class="alert alert-success">
                <div>{{Session::get('status-save')}}</div>
            </div>
        @endif

        <h2><i class="fas fa-cubes"></i> | Data Barang</h2>
        <hr>
        <button type="button" class="btn btn-primary col-md-2" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-plus-circle"> | Tambah Barang</i></button>

        <table id="myTable" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr class="text-center bg-info text-white">
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Foto Barang</th>
                    <th>Harga Barang</th>
                    <th>Stok</th>
                    <th>Keterangan</th>
                    <th>Pesan</th>
                    <th>Opsi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $x)
                <tr>
                    <td class="text-center">{{ $loop->iteration }}</td>
                    <td>{{ $x->nama_barang }}</td>
                    <td>
                        @if($x->foto_barang )
                            <img src="{{ asset('img/' . $x->foto_barang)}}" class="img-thumbnail " width="75"/>
                        @else
                            No Image
                        @endif
                    </td>
                    <td class="text-right">Rp.{{ number_format($x->harga_barang) }}</td>
                    <td class="text-center">{{ $x->stok }}</td>
                    <td>{{ $x->keterangan }}</td>
                    <td>{{ $x->pesan }}</td>
                    <td class="text-center">
                        <a href="/EditBarang/{{$x->id}}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                        <a href="/HapusBarang/{{$x->id}}" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('TambahBarang') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Nama Barang</label>
                                    <input type="text" class="form-control" name="nama_barang" id="nama">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Foto Barang</label>
                                    <input type="file" class="form-control" name="foto_barang" id="foto_barang">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Harga Barang</label>
                                    <input type="number" class="form-control" name="harga" id="harga">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Stok</label>
                                    <select name="stok" id="stok" class="form-control">
                                        <option value="0">- Pilih Stok -</option>
                                        <option value="Ada">Ada</option>
                                        <option value="Habis">Habis</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Pesan</label>
                                    <input type="text" class="form-control" name="pesan" id="pesan">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" name="ket" id="ket" cols="3" rows=2 ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> Batal</i></button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection