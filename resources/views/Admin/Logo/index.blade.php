@extends('Admin.layouts.index')

@section('content')
    <div class="col-md-12">
        <h2><i class="fas fa-image"></i> | Edit Logo</h2>
        <hr>
        @foreach ($logo as $data)
        <form action="/UpdateLogo/{{ $data->id }}" method="POST" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <center><img src="{{ asset('img/' . $data->logo) }}"  height="220px" width="280px"></center>
            </div>
            <div class="form-group">
                <label>Ganti Foto</label>
                <input type="file" name="logo" id="logo" class="form-control">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </form>
        @endforeach
    </div>
@endsection