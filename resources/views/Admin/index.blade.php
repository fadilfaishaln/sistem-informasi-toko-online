@extends('Admin.layouts.index')

@section('content')
    <div class="col-md-12">
        @foreach($logo as $x)
            <center><img src="{{ asset('img/'.$x->logo) }}" alt="" class="img-responsive img-dasbor"></center>
        @endforeach
        <div class="col-md-12">
            <h3 class="text-dasbor">Selamat Datang {{ Auth::user()->username }}</h3>
        </div>
    </div>
@endsection